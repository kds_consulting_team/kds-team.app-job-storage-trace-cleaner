'''
Created on 12. 11. 2018

@author: esner
'''
import logging
import os
import unittest
from datetime import datetime

import mock
import pytz
from freezegun import freeze_time

from component import Component


class TestComponent(unittest.TestCase):

    # set global time to 2010-10-10 - affects functions like datetime.now()
    @freeze_time("2010-10-10")
    # set KBC_DATADIR env to non-existing dir
    @mock.patch.dict(os.environ, {'KBC_DATADIR': './non-existing-dir'})
    def test_run_no_cfg_fails(self):
        with self.assertRaises(ValueError):
            comp = Component()
            comp.run()

    @mock.patch('component.KBCClient.get_orchestration_jobs')
    # set KBC_DATADIR env to test dir
    @mock.patch.dict(os.environ, {'KBC_DATADIR': os.path.join(os.path.dirname(os.path.realpath(__file__)), 'test-data')})
    def test_orchestration_job_with_no_start_time_warning(self, mock_orch_jobs):
        mock_orch_jobs.return_value = [{'startTime': None, 'runId': 123}]
        comp = Component()

        with self.assertLogs(logging.getLogger(), level='INFO') as cm:
            now = datetime.utcnow()
            res = comp.get_orchestration_job_runids(123, now)
            self.assertEqual(cm.output, ["WARNING:root:Failed to parse startTime None"])
            self.assertEqual(res, ([123], pytz.utc.localize(now).isoformat()))


if __name__ == "__main__":
    # import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
