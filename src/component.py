'''
Template Component main class.

'''

import csv
import logging
import os
import sys
from datetime import datetime

import pytz
import requests
from kbc.client_base import HttpClientBase
from kbc.env_handler import KBCEnvHandler

# configuration variables
AUDIT_LOG_FILE_NAME = 'audit_log.csv'
AUDIT_FILE_COLS = ['orchestration_config_id', 'job_run_id', 'file_id', 'file_name', 'file_created_run_id',
                   'file_created_date', 'file_deleted_date']
KEY_API_TOKEN = '#api_token'
KEY_ORCHESTRATION_ID = 'orchestration_id'
KEY_UNTIL_DATE = 'until_date'

# #### Keep for debug
KEY_DEBUG = 'debug'

MANDATORY_PARS = [KEY_UNTIL_DATE]
MANDATORY_IMAGE_PARS = []

APP_VERSION = '0.1.6'


class Component(KBCEnvHandler):

    def __init__(self, debug=False):
        KBCEnvHandler.__init__(self, MANDATORY_PARS, log_level=logging.DEBUG if debug else logging.INFO)
        # override debug from config
        if self.cfg_params.get(KEY_DEBUG):
            debug = True
        if debug:
            logging.getLogger().setLevel(logging.DEBUG)
        logging.info('Running version %s', APP_VERSION)
        logging.info('Loading configuration...')

        try:
            self.validate_config(MANDATORY_PARS)
            self.validate_image_parameters(MANDATORY_IMAGE_PARS)
        except ValueError as e:
            logging.exception(e)
            exit(1)
        # ####### EXAMPLE TO REMOVE
        token = os.getenv("KBC_TOKEN", self.cfg_params.get(KEY_API_TOKEN, None))
        if not token:
            raise ValueError("Storage token is missing!")
        stack = os.getenv("KBC_STACKID", 'connection.eu-central-1.keboola.com')

        self.client = KBCClient(token, stack)

    def run(self):
        '''
        Main execution code
        '''
        params = self.cfg_params  # noqa
        last_state = self.get_state_file()
        if last_state and last_state.get('last_timestamp'):
            from_timestamp = datetime.fromisoformat(last_state['last_timestamp'])
        else:
            from_timestamp = None

        start_date, end_date = self.get_date_period_converted(params[KEY_UNTIL_DATE], 'today')
        if params.get(KEY_ORCHESTRATION_ID):
            orchestration_id = params[KEY_ORCHESTRATION_ID]
        else:
            logging.info('Retrieving parent orchestration ID..')
            logging.debug(os.getenv('KBC_RUNID', ''))
            orchestration_id = self.client.get_conifg_by_runid(os.getenv('KBC_RUNID', '').split('.', 1)[0],
                                                               'orchestrator')

        if not orchestration_id:
            raise ValueError(
                "Parent orchestration ID not found, either place config into an orchestration "
                "or specify the ID manually.")

        logging.info(f"Getting last jobs of orchestration ID {orchestration_id} until {params[KEY_UNTIL_DATE]}")

        run_ids, last_job_timestamp = self.get_orchestration_job_runids(orchestration_id, start_date, from_timestamp)
        logging.info(run_ids)
        logging.info(f"Retrieving fileIDs from last {len(run_ids)} runs.")

        audit_log_path = os.path.join(self.tables_out_path, AUDIT_LOG_FILE_NAME)
        with open(audit_log_path, mode='w+', encoding='utf-8') as audit_f:
            audit_writer = csv.DictWriter(audit_f, fieldnames=AUDIT_FILE_COLS, lineterminator='\n')
            audit_writer.writeheader()
            del_files_cnt = 0
            for runid in run_ids:
                # get file candidates
                if params.get("use_events_api", True):
                    file_ids = self.get_ruid_files(runid)
                else:
                    file_ids = self.get_ruid_files_from_files_api(runid)
                logging.info(f"{len(file_ids)} files found in history for runID {runid}. Removing if present.")
                # delete
                deleted_files = self.client.delete_files(file_ids)
                del_files_cnt += len(deleted_files)
                # write audit
                audit_records = self.build_audit_records(orchestration_id, runid, deleted_files)
                if audit_records:
                    audit_writer.writerows(audit_records)

        self.configuration.write_table_manifest(audit_log_path, primary_key=['file_id'], incremental=True)

        self.write_state_file({"last_timestamp": last_job_timestamp})
        if del_files_cnt:
            logging.info(f"Job finished, {del_files_cnt} files deleted.")
        else:
            logging.warning("Job finished, but 0 files deleted.")

    def get_ruid_files(self, runid):
        file_ids = set()
        for e in self.client.get_storage_import_export_events(runid):
            file_ids.update(self.__retrieve_file_id(e))
        return list(file_ids)

    def get_ruid_files_from_files_api(self, runid):
        file_ids = set()
        for e in self.client.get_files(runid):
            file_ids.add(e['id'])
        return file_ids

    def __retrieve_file_id(self, event):
        ids = set()
        if event.get('results', {}).get('file', {}).get('id'):
            ids.add(event['results']['file']['id'])

        if event.get('attachments'):
            for a in event['attachments']:
                ids.add(a.get('id', ''))

        if event.get('params', {}).get('fileId', {}):
            ids.add(event['params']['fileId'])
        ids.discard('')
        return list(ids)

    def get_orchestration_job_runids(self, orchestration_id, until, from_dt=None):
        run_ids = set()
        last_job_timestamp = None
        for oj in self.client.get_orchestration_jobs(orchestration_id):

            try:
                # weird error handling
                j_time = datetime.fromisoformat(oj['startTime'])
            except TypeError:
                logging.warning(f'Failed to parse startTime {oj.get("startTime", "KeyNotPresent")}')
                logging.debug(oj)
                j_time = pytz.utc.localize(until)

            if j_time > pytz.utc.localize(until):
                continue

            if from_dt and j_time < from_dt:
                continue

            run_ids.add(oj["runId"])
            # store timestamp of the most recent job
            if not last_job_timestamp:
                last_job_timestamp = j_time.isoformat()

        return list(run_ids), last_job_timestamp

    def build_audit_records(self, orchestration_id, runid, deleted_files):
        audit_records = []
        if not deleted_files:
            return
        for f in deleted_files:
            record = {'orchestration_config_id': orchestration_id,
                      'job_run_id': runid,
                      'file_id': f['id'],
                      'file_name': f['name'],
                      'file_created_run_id': f['runId'],
                      'file_created_date': f['created'],
                      'file_deleted_date': datetime.utcnow()}
            audit_records.append(record)
        return audit_records


class KBCClient(HttpClientBase):
    URL_SUFFIXES = {"connection.keboola.com": ".keboola.com",
                    "EU": ".eu-central-1.keboola.com"}

    def __init__(self, storage_token, stack='connection.keboola.com'):
        HttpClientBase.__init__(self, 'https://syrup.' + stack.split('.', 1)[1],
                                default_http_header={"X-StorageApi-Token": storage_token})

        self.url_suffix = stack.split('.', 1)[1]

    def __get_paged_result_pages(self, url, parameters, offset=0, limit=100, max_limit=1e12):

        has_more = True
        parameters['limit'] = limit
        parameters['offset'] = offset
        while has_more:

            if offset > max_limit:
                break

            resp = self.get_raw(url, params=parameters)
            resp.raise_for_status()
            req_response = resp.json()

            if req_response:
                has_more = True
                offset += limit
                parameters['offset'] = offset
            else:
                has_more = False

            yield req_response

    def get_orchestrations(self):

        url = self.base_url + '/orchestrator/orchestrations'
        res = self.get_raw(url)
        return res

    def get_orchestration_jobs(self, orchestration_id):
        url = self.base_url + f'/orchestrator/orchestrations/{orchestration_id}/jobs'
        for p in self.__get_paged_result_pages(url, {}, max_limit=9999):
            for j in p:
                yield j

    def get_storage_import_export_events(self, run_id, until=None, since=None):
        url = f'https://connection.{self.url_suffix}/v2/storage/events'
        parameters = {"component": "storage",
                      "runId": run_id,
                      "q": "event:storage.fileUploaded",
                      "since": since,
                      "until": until}
        for p in self.__get_paged_result_pages(url, parameters):
            for e in p:
                yield e

    def get_files(self, run_id, until=None, since=None):
        url = f'https://connection.{self.url_suffix}/v2/storage/files'

        q = None
        if until:
            q = f'created:<{until}'
        if since:
            if q:
                q += q + ' AND '
            q += f'created:>{since}'

        parameters = {"component": "storage",
                      "runId": run_id}
        if q:
            parameters['q'] = q
        for p in self.__get_paged_result_pages(url, parameters):
            for e in p:
                yield e

    def get_conifg_by_runid(self, run_id, component=None):
        url = f'https://syrup.{self.url_suffix}/queue/jobs'
        parameters = {"runId": run_id,
                      "component": component}
        resp = self.get_raw(url, params=parameters)
        resp.raise_for_status()
        req_response = resp.json()
        if req_response:
            return req_response[0].get('params', {}).get('config', None)
        else:
            return None

    def get_file_detail(self, file_id):
        url = f'https://connection.{self.url_suffix}/v2/storage/files/{file_id}'
        r = self.get_raw(url)
        if r.status_code == 404:
            logging.warning(f"File ID {file_id} not found, it was already deleted.")
            return
        elif r.status_code >= 300:
            r.raise_for_status()
        return r.json()

    def delete_file(self, file_id):
        file_detail = self.get_file_detail(file_id)
        if not file_detail:
            return None

        url = f'https://connection.{self.url_suffix}/v2/storage/files/{file_id}'
        s = requests.Session()
        s.auth = self._auth

        headers = self._auth_header
        s.headers.update(headers)
        r = self.requests_retry_session(session=s).request('DELETE', url=url)
        r.raise_for_status()
        return file_detail

    def delete_files(self, file_ids):
        removed_files = []
        for f in file_ids:
            f = self.delete_file(f)
            if f:
                removed_files.append(f)
        return removed_files


"""
        Main entrypoint
"""
if __name__ == "__main__":
    if len(sys.argv) > 1:
        debug_arg = sys.argv[1]
    else:
        debug_arg = False
    try:
        comp = Component(debug_arg)
        comp.run()
    except Exception as exc:
        logging.exception(exc)
        exit(1)
